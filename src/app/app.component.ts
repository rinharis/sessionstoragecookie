import { Component } from '@angular/core';
import { CookieService } from 'angular2-cookie/core';
import { environment } from 'src/environments/environment.prod';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private _cookieService: CookieService, private localSt:LocalStorageService, private sessionSt:SessionStorageService) { }
  envName = environment.production;
  channelName = 'CodeSpace';

  setCookies() {
    this._cookieService.put('test', 'testing cookie');
  }
  getCookies() {
    alert(this._cookieService.get("test"));
  }

  delCookies() {
    this._cookieService.remove("test");
  }
  setLocalStorage(){
    this.localSt.store("userName", "Gourab Paul");
  }
  getLocalStorage(){
    alert(this.localSt.retrieve("userName"));
  }
  delLocalStorage(){
    this.localSt.clear("boundValue");
  }
  setSessionStorage(){
    this.sessionSt.store("logged-in", "user");
  }
  getSessionStorage(){
    alert(this.sessionSt.retrieve("logged-in"));
  }
  delSessionStorage(){
    this.sessionSt.clear("logged-in");
  }
}
