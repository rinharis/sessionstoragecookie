import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import {CookieService} from 'angular2-cookie/services/cookies.service';
import {Ng2Webstorage} from 'ngx-webstorage';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    Ng2Webstorage,

  ],

  providers: [CookieService],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
